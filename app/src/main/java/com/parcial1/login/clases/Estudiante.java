package com.parcial1.login.clases;

import java.util.Date;

public class Estudiante {
    private String cedula;
    private String grupo;
    private  int monedas;
    private  int tiempoJugado;
    private Date fechaRegistro;
    private Date ultimoAcceso;

    public Estudiante(String _cedula, String _grupo){
        cedula = _cedula;
        grupo = _grupo;
    }

    public String getCedula() {
        return cedula;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public Date getUltimoAcceso() {
        return ultimoAcceso;
    }

    public int getMonedas() {
        return monedas;
    }

    public int getTiempoJugado() {
        return tiempoJugado;
    }

    public void setUltimoAcceso(Date ultimoAcceso) {
        this.ultimoAcceso = ultimoAcceso;
    }

    public void setMonedas(int monedas) {
        this.monedas = monedas;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public void setTiempoJugado(int tiempoJugado) {
        this.tiempoJugado = tiempoJugado;
    }
}

