package com.parcial1.login.clases;

public class Usuario {
    private String cedula;
    private String user;//??? que es esto.
    private String email;
    private String type;
    private String password;

    public Usuario(String _cedula, String _user, String _email, String _type, String _password){
        cedula = _cedula;
        user = _user;
        email = _email;
        type = _type;
        password = _password;
    }
    public String getType() {
        return type;
    }

    public String getCedula() {
        return cedula;
    }

    public String getEmail() {
        return email;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

}

