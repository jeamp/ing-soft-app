package com.parcial1.login.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.widget.Toast;

import com.parcial1.login.clases.Usuario;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class UsuarioQuery {
    static private SQLiteDatabase db;
    static private final int version = 3;
    static private final String DATABASE_NAME = "Login.db";

    public static boolean Agregar(Context context, Usuario usuario) {
        UsuariosSQLiteHelper userHelper = new UsuariosSQLiteHelper(context, DATABASE_NAME, null, version);
        db = userHelper.getWritableDatabase();
        try {
            if (db != null) {
//                db.execSQL(String.format(Locale.US, "INSERT INTO usuarios(user, password, type) " +
//                        "VALUES ('%s', '%s', '%s')", usuario.getUser(), usuario.getPassword(), usuario.getType()));
                ContentValues dataToInsert = new ContentValues();
                dataToInsert.put("user", usuario.getUser());
                dataToInsert.put("cedula", usuario.getCedula());
                dataToInsert.put("email", usuario.getEmail());
                dataToInsert.put("password", usuario.getPassword());
                dataToInsert.put("type", usuario.getType());

                db.insert("usuarios", null, dataToInsert);
                db.close();
                System.out.println("DB DONE");
                return true;
            } else {
                System.out.println("ERROR DB");
                Toast.makeText(context, "No se pudo agregar a la base de datos.", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            System.out.println("ERROR D2B");
            System.out.println(e);
            return false;
        }
    }

    public static boolean Actualizar(Context context, Usuario usuario) {
        UsuariosSQLiteHelper userHelper = new UsuariosSQLiteHelper(context, DATABASE_NAME, null, version);
        db = userHelper.getWritableDatabase();
        try {
            if (db != null) {
                ContentValues dataToInsert = new ContentValues();
                if (!usuario.getEmail().isEmpty())
                    dataToInsert.put("email", usuario.getEmail());
                if (!usuario.getPassword().isEmpty())
                    dataToInsert.put("password", usuario.getPassword());
                if (!usuario.getUser().isEmpty())
                    dataToInsert.put("user", usuario.getUser());

                if (dataToInsert.size() == 0) return false;

                int rows = db.update("usuarios", dataToInsert, "cedula=?", new String[]{usuario.getCedula()});


                db.close();
                if (rows >= 1)
                    return true;
                else
                    return false;
            } else {
                Toast.makeText(context, "No se pudo actualizar la base de datos.", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public static boolean Validar(Context context, String cedula, String password) {
        UsuariosSQLiteHelper userHelper = new UsuariosSQLiteHelper(context, DATABASE_NAME, null, version);
        db = userHelper.getWritableDatabase();
        try {
            if (db != null) {
                Cursor c = db.query("usuarios", new String[]{"COUNT(cedula) cant"}, "cedula=? AND password=?", new String[]{cedula, password}, null, null, null);
                if (!c.moveToFirst()) {
                    c.close();
                    db.close();
                    return false;
                }

                System.out.println(c.getString(0));

                if (c.getInt(0) != 1) {
                    c.close();
                    db.close();
                    return false;
                }

                return true;
            } else {
                Toast.makeText(context, "No se pudo validar el usuario en la base de datos.", Toast.LENGTH_LONG).show();
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public static Map<?, ?> Buscar(Context context, String[] campos, String cedula) {

        UsuariosSQLiteHelper userHelper = new UsuariosSQLiteHelper(context, DATABASE_NAME, null, version);
        db = userHelper.getWritableDatabase();
        try {
            if (db != null) {
                Cursor c = db.query("usuarios", campos, "cedula=?", new String[]{cedula}, null, null, null);
                if (!c.moveToFirst()) {
                    c.close();
                    db.close();
                    return null;
                }
                Map<String, String> userMap = new HashMap<>();
                System.out.println(c.getString(0));
                for (int i = 0; i < c.getColumnCount(); i++) {
                    userMap.put(campos[i], c.getString(i));
                }

                return userMap;
            } else {
                Toast.makeText(context, "No se pudo validar el usuario en la base de datos.", Toast.LENGTH_LONG).show();
                return null;
            }
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
    }
//    private String username, password, type;
//    public Usuario(String u, String p, String t){
//        username = u;
//        password = p;
//        type = t;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public String getUsername() {
//        return username;
//    }
}
