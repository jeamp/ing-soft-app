package com.parcial1.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.parcial1.login.clases.Usuario;
import com.parcial1.login.helper.CustomDialog;
import com.parcial1.login.helper.UsuarioQuery;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RegistroActivity extends AppCompatActivity {
    EditText cedula, email, user, password, password2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
        InitializeControllers();

    }


    private void InitializeControllers() {
        cedula= (EditText) findViewById(R.id.cedulaRegistro);
        user = (EditText) findViewById(R.id.userRegistro);
        email = (EditText) findViewById(R.id.emailRegistro);
        password = (EditText) findViewById(R.id.passwordRegistro);
        password2 = (EditText) findViewById(R.id.passwordRegistro2);
    }

    public boolean Registrar(View v) {

        if(cedula.getText().toString().equals("")
        || !cedula.getText().toString().contains("-")
        || cedula.getText().length() <= 8) {

            CustomDialog cd = new CustomDialog("Error", "Cedula debe ser valida");
            cd.show(getSupportFragmentManager(),"OK");
            return false;
//            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
        }
        if(user.getText().toString().equals("")) {

            CustomDialog cd = new CustomDialog("Error", "Nombre no puede estar vacio");
            cd.show(getSupportFragmentManager(),"OK");
            return false;
//            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
        }
        if(user.getText().length() < 3) {
            CustomDialog cd = new CustomDialog("Error", "Nombre no puede ser menos de 3 caracteres");
            cd.show(getSupportFragmentManager(), "OK");
            return false;
//            Toast.makeText(this, "Name can't be less than 3 characters", Toast.LENGTH_SHORT).show();
        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@") || !email.getText().toString().contains(".")) {

            CustomDialog cd = new CustomDialog("Error", "Debe ingresar un email valido");
            cd.show(getSupportFragmentManager(), "OK");
            return false;

//            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
        }
        if (password.getText().toString().equals("") || password2.getText().toString().equals("")) {
            CustomDialog cd = new CustomDialog("Error", "Password no puede estar vacio");
            cd.show(getSupportFragmentManager(), "OK");
            return false;
//            Toast.makeText(this, "Password can't be empty", Toast.LENGTH_SHORT).show();
        }
        if (!password2.getText().toString().equals(password.getText().toString())) {
            CustomDialog cd = new CustomDialog("Error", "Los passwords deben ser iguales.");
            cd.show(getSupportFragmentManager(), "OK");
            return false;
//            Toast.makeText(this, "Passwords must match", Toast.LENGTH_SHORT).show();
        }
       Map<?,?> value = (Map<?,?>)UsuarioQuery.Buscar(getApplicationContext(),new String[]{"cedula"}, cedula.getText().toString());
        try{
            if(value.size() != 0 ) {

                CustomDialog cd = new CustomDialog("Error", "Cedula Ya existe.");
                cd.show(getSupportFragmentManager(), "OK");
                return false;
            }
        } catch (NullPointerException ne){
            
        }
        Usuario nuevoUsuario = new Usuario(cedula.getText().toString(), user.getText().toString(),email.getText().toString(),"estudiante", password.getText().toString());
        UsuarioQuery.Agregar(getApplicationContext(),nuevoUsuario);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        return false;
    }
}
