package com.parcial1.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.parcial1.login.helper.CustomDialog;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class CrearUsuariosActivity extends AppCompatActivity {
    EditText email;
    EditText password, password2;
    Spinner tipoUsuario;
    SharedPreferences sharedPreferences;
    String saveType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuentas);
        InitializeControllers();
        InitializeSpinner();
        InitializeData();

    }

    private void InitializeData() {
        Intent lastPageIntent = getIntent();
        saveType = lastPageIntent.getStringExtra("saveType");
    }

    private void InitializeControllers() {
//        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        password2 = (EditText) findViewById(R.id.password2);
        tipoUsuario = (Spinner) findViewById(R.id.spTipoUsuario);
    }

    private void InitializeSpinner() {

        List<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Administrador");
        spinnerArray.add("Usuario Normal");
        spinnerArray.add("Registrador");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spinnerArray);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        tipoUsuario.setAdapter(adapter);
    }

    public boolean CreateAccount(View v) {
//        if(name.getText().toString().equals("")) {
//
//            CustomDialog cd = new CustomDialog("Error", "Name can't be empty");
//            cd.show(getSupportFragmentManager(),"OK");
//            return false;
////            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
//        }
//        if(name.getText().length() < 3) {
//            CustomDialog cd = new CustomDialog("Error", "Name can't be less than 3 characters");
//            cd.show(getSupportFragmentManager(), "OK");
//            return false;
////            Toast.makeText(this, "Name can't be less than 3 characters", Toast.LENGTH_SHORT).show();
//        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@") || !email.getText().toString().contains(".")) {

            CustomDialog cd = new CustomDialog("Error", "Must be a valid email");
            cd.show(getSupportFragmentManager(), "OK");
            return false;

//            Toast.makeText(this, "Name can't be empty", Toast.LENGTH_SHORT).show();
        }
        if (password.getText().toString().equals("") || password2.getText().toString().equals("")) {
            CustomDialog cd = new CustomDialog("Error", "Password can't be empty");
            cd.show(getSupportFragmentManager(), "OK");
            return false;
//            Toast.makeText(this, "Password can't be empty", Toast.LENGTH_SHORT).show();
        }
        if (!password2.getText().toString().equals(password.getText().toString())) {
            CustomDialog cd = new CustomDialog("Error", "Passwords must match");
            cd.show(getSupportFragmentManager(), "OK");
            return false;
//            Toast.makeText(this, "Passwords must match", Toast.LENGTH_SHORT).show();
        }

        Intent intent = new Intent(this, LoginActivity.class);

        if (saveType.equals("sharedPreferences")) {

            try {
                sharedPreferences = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
//                if (sharedPreferences.getAll().size() == 0) {
                    SharedPreferences.Editor spUsuario = sharedPreferences.edit();
//            spUsuario.putString("name", name.getText().toString());
                    spUsuario.putString("user", email.getText().toString());
                    spUsuario.putString("pass", password.getText().toString());
                    spUsuario.putString("tipo", tipoUsuario.getSelectedItem().toString());
                    spUsuario.apply();//commit es instantaneo y no en el background
//                } else {
//                    CustomDialog cd = new CustomDialog("Error", "Email has been used already.");
//                    cd.show(getSupportFragmentManager(), "OK");
//                    return false;
//                }

            } catch (Exception e) {
                System.out.println(e);
            }

        } else if (saveType.equals("file")) {
            try {
                OutputStreamWriter writer = new OutputStreamWriter(openFileOutput("login.txt", Context.MODE_PRIVATE));
                writer.write(email.getText().toString() + ":" + password.getText().toString() + ":" + tipoUsuario.getSelectedItem().toString());
                writer.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            intent.putExtra("tipo", tipoUsuario.getSelectedItem().toString());
            intent.putExtra("user", email.getText().toString());
            intent.putExtra("pass", password.getText().toString());
        }
        startActivity(intent);
        return false;
    }
}
