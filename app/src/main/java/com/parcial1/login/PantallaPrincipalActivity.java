package com.parcial1.login;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parcial1.login.helper.CustomDialog;
import com.parcial1.login.helper.UsuarioQuery;

import java.util.HashMap;

public class PantallaPrincipalActivity extends AppCompatActivity {
    Button jugarBtn, crearReglasBtn, mantenimientoPreguntasBtn, rankingBtn, gestionarUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);
        InicializarControladores();
        AjustarSegunUsuario();
    }

    private void InicializarControladores() {
        jugarBtn = (Button) findViewById(R.id.jugarBtn);
        crearReglasBtn = (Button) findViewById(R.id.crearReglasBtn);
        mantenimientoPreguntasBtn = (Button) findViewById(R.id.mantenimientoPreguntasBtn);
        rankingBtn = (Button) findViewById(R.id.rankingBtn);
        gestionarUsuarios = (Button) findViewById(R.id.gestionarUsuarios);
    }
    
    private void AjustarSegunUsuario() {
        //usar sql.
        Intent i = getIntent();
        HashMap<?, ?> values = (HashMap<?, ?>) UsuarioQuery.Buscar(getApplicationContext(), new String[]{"type"}, i.getStringExtra("cedula"));
        try {
            if (values.get("type").equals("admin")) {
                jugarBtn.setVisibility(View.GONE);
                crearReglasBtn.setVisibility(View.GONE);
                mantenimientoPreguntasBtn.setVisibility(View.GONE);
                rankingBtn.setVisibility(View.GONE);
                gestionarUsuarios.setVisibility(View.VISIBLE);
            } else if (values.get("type").equals("profesor")) {
                jugarBtn.setVisibility(View.GONE);
                crearReglasBtn.setVisibility(View.VISIBLE);
                mantenimientoPreguntasBtn.setVisibility(View.VISIBLE);
                rankingBtn.setVisibility(View.VISIBLE);
                gestionarUsuarios.setVisibility(View.GONE);
            } else if (values.get("type").equals("estudiante")) {
                jugarBtn.setVisibility(View.VISIBLE);
                crearReglasBtn.setVisibility(View.GONE);
                mantenimientoPreguntasBtn.setVisibility(View.GONE);
                rankingBtn.setVisibility(View.VISIBLE);
                gestionarUsuarios.setVisibility(View.GONE);
            }
        } catch (NullPointerException ne) {
            System.out.println("No Type was gotten. we've been pranked.");
        }
    }
    public void Volver(View v){
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }
}
