package com.parcial1.login;

import  com.parcial1.gestionar.SelectUserType;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.parcial1.gestionar.SelectUserType;
import com.parcial1.login.clases.Usuario;
import com.parcial1.login.helper.CustomDialog;
import com.parcial1.login.helper.UsuarioQuery;
import com.parcial1.login.helper.UsuariosSQLiteHelper;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Locale;


public class LoginActivity extends AppCompatActivity {
    String email = "1@1.com", password = "pog", type = "admin";
    EditText cedulaET, passwordET;
    Button login, olvido;
    TextView mensajeError;

    //    UsuarioQuery usuarioQuery = new UsuarioQuery();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        InitializeControllers();
        Usuario u = new Usuario("e-8-174461", "banana", email, type, password);
//        UsuarioQuery.Agregar(getApplicationContext(), u);
    }


    private void InitializeControllers() {
        cedulaET = (EditText) findViewById(R.id.cedula);
        passwordET = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.registerBtn);
        olvido = (Button) findViewById(R.id.olvidoBtn);
        mensajeError = (TextView) findViewById(R.id.errorMessage);
    }

    public void Register(View v) {
        startActivity(new Intent(this, RegistroActivity.class));
    }
    public void Gestionar(View v) { startActivity(new Intent(this, SelectUserType.class)); }


    public void Login(View v) {
        String ced = cedulaET.getText().toString();
        String pas = passwordET.getText().toString();
        if (ced.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Campo Usuario es requerido.", Toast.LENGTH_LONG).show();
//            mensajeError.setText("Campo Usuario es requerido.");
//            mensajeError.setVisibility(View.VISIBLE);
        } else if (pas.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Campo Password es requerido.", Toast.LENGTH_LONG).show();
//            mensajeError.setText("Campo password es requerido.");
//            mensajeError.setVisibility(View.VISIBLE);
        } else if (UsuarioQuery.Validar(getApplicationContext(), ced, pas)) {
            System.out.println("logged in");
            Intent intent = new Intent(this, PantallaPrincipalActivity.class);
            intent.putExtra("cedula", cedulaET.getText().toString());
            cedulaET.setText("");
            passwordET.setText("");
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Credenciales Invalidas", Toast.LENGTH_LONG).show();
//            mensajeError.setText("Credenciales Invalidas");
//            mensajeError.setVisibility(View.VISIBLE);
        }

    }

}