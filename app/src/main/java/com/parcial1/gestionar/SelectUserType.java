package com.parcial1.gestionar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.parcial1.login.R;

public class SelectUserType extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_user_type);
    }

    public void GestionarEstudiante (View v) { startActivity(new Intent(this, GestionarEstudiante.class)); }
}