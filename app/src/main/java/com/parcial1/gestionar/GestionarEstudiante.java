package com.parcial1.gestionar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.parcial1.login.R;

public class GestionarEstudiante extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gestionar_estudiante);
    }
    public void Detalles (View v) { startActivity(new Intent(this, userDetail.class)); }
    public void Agregar (View v) { startActivity(new Intent(this, AgregarUser.class)); }
    public void Editar (View v) { startActivity(new Intent(this, EditUser.class)); }
}